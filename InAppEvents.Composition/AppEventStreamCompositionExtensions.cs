﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Linq;

namespace InAppEvents
{
    /// <summary>
    /// Extension methods for loading plugin event handlers.
    /// </summary>
    public static class AppEventStreamCompositionExtensions
    {
        /// <summary>
        /// Loads instances of <see cref="IAppEventHandler"/> from the provided composition container
        /// and invoke the <see cref="IAppEventHandler.Subscribe"/> method to allow each handler
        /// component to register its event handlers.
        /// </summary>
        /// <param name="eventStream">The event stream to add the handlers to.</param>
        /// <param name="compositionContainer">The container responsible for locating and loading the
        /// concrete implementations of <see cref="IAppEventHandler"/>.</param>
        /// <param name="onHandlerCreatedAction">A callback action that will be invoked for each of the loaded
        /// handlers before they are added to the event stream. Pass null if you don't require such
        /// functionality.</param>
        /// <exception cref="ArgumentNullException"><paramref name="compositionContainer"/> is null.</exception>
        public static void LoadHandlers(this IAppEventStream eventStream, CompositionContainer compositionContainer, Action<IAppEventHandler> onHandlerCreatedAction)
        {
            Ensure.ParamIsNotNull(compositionContainer, "compositionContainer");
            var exports = compositionContainer.GetExports<IAppEventHandler>();
            var handlers = exports.Select(lazyHandler => lazyHandler.Value).ToList();
            if (onHandlerCreatedAction != null)
            {
                handlers.ForEach(onHandlerCreatedAction);
            }
            eventStream.AddHandlers(handlers.ToArray());
        }
    }
}
