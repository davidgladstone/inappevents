﻿using System.Reflection;

[assembly: AssemblyTitle("InAppEvents.Composition")]
[assembly: AssemblyDescription("Supports discovering event handlers at runtime.")]