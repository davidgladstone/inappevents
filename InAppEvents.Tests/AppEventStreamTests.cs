﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using InAppEvents.Tests.Support;
using Xunit;

namespace InAppEvents.Tests
{
    public class AppEventStreamTests
    {
        [Fact]
        public void Ctr_NoExceptionHandler_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new AppEventStream(null));
        }

        [Fact]
        public void Raise_GivenCriticalEventMarkedAsHandled_ThrowsCriticalEventHandlingException()
        {
            var stream = new AppEventStream(e => { });
            var sampleEvent = new SampleCriticalEvent();
            sampleEvent.MarkAsHandled();
            Assert.Throws<CriticalEventHandlingException>(() => stream.Raise(sampleEvent));
        }

        [Fact]
        public void Raise_GivenCriticalEventAndNoHandler_ThrowsCriticalEventHandlingException()
        {
            var stream = new AppEventStream(e => { });
            var sampleEvent = new SampleCriticalEvent();
            Assert.Throws<CriticalEventHandlingException>(() => stream.Raise(sampleEvent));
        }

        [Fact]
        public void Raise_GivenNonCriticalEventAndNoHandler_DoesNotThrow()
        {
            var stream = new AppEventStream(e => { });
            var sampleEvent = new SampleNonCriticalEvent();
            Assert.DoesNotThrow(() => stream.Raise(sampleEvent));
        }

        [Fact]
        public void Raise_GivenCriticalEventHandlerDoesNotMarkAsHandledAndAmbientTransaction_ThrowsCriticalEventHandlingExceptionAndAbortsTx()
        {
            using (new TransactionScope(TransactionScopeOption.Required, TestTransactionOptions))
            {
                Assert.Equal(TransactionStatus.Active, Transaction.Current.TransactionInformation.Status);

                var stream = new AppEventStream(e => { });
                var sampleEvent = new SampleCriticalEvent();
                var wasExecuted = false;
                stream.TransactionalHandler<SampleCriticalEvent>(evt => wasExecuted = true);

                Assert.Throws<CriticalEventHandlingException>(() => stream.Raise(sampleEvent));
                Assert.True(wasExecuted);

                Assert.Equal(TransactionStatus.Aborted, Transaction.Current.TransactionInformation.Status);
            }
        }

        [Fact]
        public void Raise_GivenCriticalEventAndTxHandlerMarksEventAsHandled_DoesNotThrow()
        {
            var stream = new AppEventStream(e => { });
            var sampleEvent = new SampleCriticalEvent();
            var wasExecuted = false;
            stream.TransactionalHandler<SampleCriticalEvent>(evt =>
            {
                evt.MarkAsHandled();
                wasExecuted = true;
            });

            Assert.DoesNotThrow(() => stream.Raise(sampleEvent));
            Assert.True(wasExecuted);
        }

        [Fact]
        public void Raise_GivenCriticalEventAndIndependentHandlerMarksEventAsHandled_ThrowsCriticalEventHandlingException()
        {
            var stream = new AppEventStream(e => { });
            var sampleEvent = new SampleCriticalEvent();
            var wasExecuted = false;
            stream.IndependentHandler<SampleCriticalEvent>(evt =>
            {
                evt.MarkAsHandled();
                wasExecuted = true;
            });

            Assert.Throws<CriticalEventHandlingException>(() => stream.Raise(sampleEvent));
            Assert.True(wasExecuted);
        }

        [Fact]
        public void Raise_GivenTxHandlerAndNoAmbientTransactionScope_InvokesHandlerInNewTransactionAndCommits()
        {
            Assert.Null(Transaction.Current);

            var stream = new AppEventStream(e => { });
            Transaction tx = null;
            bool isCommitted = false;
            stream.TransactionalHandler<SampleNonCriticalEvent>(evt =>
            {
                tx = Transaction.Current;
                Assert.Equal(TransactionStatus.Active, tx.TransactionInformation.Status);
                tx.TransactionCompleted += (e, a) => isCommitted = true;
            });

            Assert.DoesNotThrow(() => stream.Raise(new SampleNonCriticalEvent()));
            Assert.NotNull(tx);
            Assert.True(isCommitted);
        }

        [Fact]
        public void Raise_GivenTxHandlerAndAmbientTransaction_InvokesHandlerInExistingTransactionWithoutCommitting()
        {
            Assert.Null(Transaction.Current);
            using (new TransactionScope(TransactionScopeOption.Required, TestTransactionOptions))
            {
                var ambientTx = Transaction.Current;
                Assert.NotNull(ambientTx);

                var wasExecuted = false;
                var stream = new AppEventStream(e => { });
                stream.TransactionalHandler<SampleNonCriticalEvent>(evt =>
                {
                    Assert.Same(ambientTx, Transaction.Current);
                    wasExecuted = true;
                });

                Assert.DoesNotThrow(() => stream.Raise(new SampleNonCriticalEvent()));
                Assert.True(wasExecuted);
                Assert.Equal(TransactionStatus.Active, ambientTx.TransactionInformation.Status);
            }
        }

        private static TransactionOptions TestTransactionOptions
        {
            get { return new TransactionOptions {IsolationLevel = IsolationLevel.RepeatableRead}; }
        }

        [Fact]
        public void Raise_GivenTxHandlerFails_ThrowsAndAbortsTransaction()
        {
            Assert.Null(Transaction.Current);
            using (new TransactionScope(TransactionScopeOption.Required, TestTransactionOptions))
            {
                var ambientTx = Transaction.Current;
                Assert.NotNull(ambientTx);

                var wasExecuted = false;
                var stream = new AppEventStream(e => { });
                stream.TransactionalHandler<SampleNonCriticalEvent>(evt =>
                {
                    Assert.Same(ambientTx, Transaction.Current);
                    wasExecuted = true;
                    throw new InvalidOperationException("Failed");
                });

                Assert.Throws<InvalidOperationException>(() => stream.Raise(new SampleNonCriticalEvent()));
                Assert.True(wasExecuted);
                Assert.Equal(TransactionStatus.Aborted, ambientTx.TransactionInformation.Status);
            }
        }

        [Fact]
        public void Raise_GivenTxHandlerAndIndependentHandler_IndependentHandlerIsInvokedLast()
        {
            Assert.Null(Transaction.Current);

            var txWorkExecuted = false;
            var independentWorkExecuted = false;
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<SampleNonCriticalEvent>(evt =>
            {
                txWorkExecuted = true;
                Assert.False(independentWorkExecuted);
            });
            stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
            {
                independentWorkExecuted = true;
                Assert.True(txWorkExecuted);
            });

            Assert.DoesNotThrow(() => stream.Raise(new SampleNonCriticalEvent()));
            Assert.True(txWorkExecuted);
            Assert.True(independentWorkExecuted);
        }

        [Fact]
        public void Raise_GivenThrowingTxHandlerAndIndependentHandler_IndependentWorkIsStillPerformed()
        {
            Assert.Null(Transaction.Current);

            var txWorkExecuted = false;
            var independentWorkExecuted = false;
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<SampleNonCriticalEvent>(evt =>
            {
                txWorkExecuted = true;
                Assert.False(independentWorkExecuted);
                throw new InvalidOperationException();
            });
            stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
            {
                independentWorkExecuted = true;
                Assert.True(txWorkExecuted);
            });

            Assert.Throws<InvalidOperationException>(() => stream.Raise(new SampleNonCriticalEvent()));
            Assert.True(txWorkExecuted);
            Assert.True(independentWorkExecuted);
        }

        [Fact]
        public void Raise_GivenTwoFailingIndependentHandlers_BothAreExecutedAndExceptionsPassedToExceptionAction()
        {
            var exception1 = new Exception("1");
            var exception2 = new Exception("2");

            var workExecutedCount = 0;
            var exceptions = new List<Exception>();
            var stream = new AppEventStream(exceptions.Add);

            stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
            {
                workExecutedCount++;
                throw exception1;
            });
            stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
            {
                workExecutedCount++;
                throw exception2;
            });

            Assert.DoesNotThrow(() => stream.Raise(new SampleNonCriticalEvent()));
            Assert.Equal(2, workExecutedCount);
            Assert.Equal(2, exceptions.Count);
            Assert.Contains(exception1, exceptions);
            Assert.Contains(exception2, exceptions);
        }

        [Fact]
        public void Raise_ExceptionHandlerThrows_ExceptionBubblesToCallerOfRaise()
        {
            var exception1 = new Exception("1");
            var exception2 = new Exception("2");

            var workExecutedCount = 0;
            var exceptions = new List<Exception>();
            var stream = new AppEventStream(e =>
            {
                exceptions.Add(e);
                throw new InvalidOperationException();
            });

            stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
            {
                workExecutedCount++;
                throw exception1;
            });
            stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
            {
                workExecutedCount++;
                throw exception2;
            });

            Assert.Throws<InvalidOperationException>(() => stream.Raise(new SampleNonCriticalEvent()));
            Assert.Equal(1, workExecutedCount);
            Assert.Equal(1, exceptions.Count);
        }

        [Fact]
        public void Raise_GivenIndependentHandlerThatThrowsAndExceptionHandlerThrows_IsNotInvokedInTheTransaction()
        {
            using (new TransactionScope(TransactionScopeOption.Required))
            {
                Assert.NotNull(Transaction.Current);

                var workExecutedCount = 0;
                var stream = new AppEventStream(e => { });
                stream.IndependentHandler<SampleNonCriticalEvent>(evt =>
                {
                    workExecutedCount++;
                    Assert.Null(Transaction.Current);
                });

                Assert.DoesNotThrow(() => stream.Raise(new SampleNonCriticalEvent()));
                Assert.Equal(1, workExecutedCount);
            }
        }

        [Fact]
        public void AddHandlers_GivenNullArray_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(()=>stream.AddHandlers(null));
        }

        [Fact]
        public void AddHandlers_GivenNullHandlerInArray_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentException>(()=>stream.AddHandlers(new TestAppEventHandler(), null));
        }

        [Fact]
        public void AddHandlers_GivenNewHandlers_InvokesSubscribeOnEach()
        {
            var handlers = new List<TestAppEventHandler> {new TestAppEventHandler(), new TestAppEventHandler()};
            var stream = new AppEventStream(e => { });
            stream.AddHandlers(handlers.Cast<IAppEventHandler>().ToArray());
            Assert.Empty(handlers.Where(h=>!h.IsSubscribed));
        }

        [Fact]
        public void AddHandlers_GivenExistingHandlers_InvokesSubscribeOnlyOnHandlersThatHaveNotAlreadyBeenSubscribed()
        {
            var handlers = new List<TestAppEventHandler> { new TestAppEventHandler(), new TestAppEventHandler(), new TestAppEventHandler() };
            var stream = new AppEventStream(e => { });
            stream.AddHandlers(handlers[0], handlers[1]);
            stream.AddHandlers(handlers[1], handlers[2]);
            Assert.Empty(handlers.Where(h => !h.IsSubscribed));
        }

        [Fact]
        public void AddHandlers_GivenSameHandlerMultipleTimes_InvokesSubscribeOnlyOnce()
        {
            var handler = new TestAppEventHandler();
            var stream = new AppEventStream(e => { });
            stream.AddHandlers(handler, handler);
            Assert.True(handler.IsSubscribed);
        }


        [Fact]
        public void TestHandlers_SubscribedTwice_Throws()
        {
            // Other tests rely on the behaviour of the Subscribe method of this test class 
            // so we're making sure it doesn't allow multiple calls.
            var handler = new TestAppEventHandler();
            handler.Subscribe(null);
            Assert.Throws<InvalidOperationException>(()=>handler.Subscribe(null));
        }

        [Fact]
        public void TransactionalHandler_PassedNullAction_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(()=>stream.TransactionalHandler<object>(null));
        }

        [Fact]
        public void TransactionalHandler_PassedPredicateAndNullAction_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(() => stream.TransactionalHandler<object>(e => true, null));
        }

        [Fact]
        public void TransactionalHandler_PassedNullPredicateAndAction_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(() => stream.TransactionalHandler<object>(null, e => { }));
        }

        [Fact]
        public void IndependentHandler_PassedNullAction_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(() => stream.IndependentHandler<object>(null));
        }

        [Fact]
        public void IndependentHandler_PassedPredicateAndNullAction_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(() => stream.IndependentHandler<object>(e => true, null));
        }

        [Fact]
        public void IndependentHandler_PassedNullPredicateAndAction_ThrowsArgumentNullException()
        {
            var stream = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(() => stream.IndependentHandler<object>(null, e => { }));
        }

        [Fact]
        public void IndependentHandler_PassedPredicateAndAction_FiltersEventsUsingThePredicate()
        {
            var stream = new AppEventStream(e => { });
            var handledEvents = new List<SampleNonCriticalEvent>();
            stream.IndependentHandler<SampleNonCriticalEvent>(e=>e.Value < 10, handledEvents.Add);

            stream.Raise(new SampleNonCriticalEvent { Value = 100 }); // Shouldn't be handled
            stream.Raise(new SampleNonCriticalEvent { Value = 1 }); // Should be handled

            Assert.Equal(1, handledEvents.Count);
            Assert.Equal(1, handledEvents[0].Value);
        }

        [Fact]
        public void IndependentHandler_PredicateThatThrows_DoesNeitherThrowsNorPreventOtherIndependentHandlersFromRunning()
        {
            int predicateCount = 0;
            int handlerCount = 0;
            var exceptions = new List<Exception>();
            var stream = new AppEventStream(exceptions.Add);
            Assert.DoesNotThrow(() => 
                stream.IndependentHandler<object>(
                    e =>
                    {
                        predicateCount++;
                        throw new Exception("predicate1");
                    }, 
                    e => { handlerCount++; }));
            Assert.DoesNotThrow(() => 
                stream.IndependentHandler<object>(
                    e =>
                    {
                        predicateCount++;
                        throw new Exception("predicate2");
                    }, 
                    e => { handlerCount++; }));

            Assert.DoesNotThrow(()=>stream.Raise(new object()));
            Assert.Equal(2, predicateCount);
            Assert.Equal(0, handlerCount);
            Assert.Equal(2, exceptions.Count);
        }

        [Fact]
        public void TransactionHandler_PredicateThatThrows_RaisesExceptionToCallerAndAbortsTransaction()
        {
            using (new TransactionScope(TransactionScopeOption.Required, TestTransactionOptions))
            {
                Assert.NotNull(Transaction.Current);
                Assert.Equal(TransactionStatus.Active, Transaction.Current.TransactionInformation.Status);

                int predicateCount = 0;
                int handlerCount = 0;
                var stream = new AppEventStream(e => { });
                Assert.DoesNotThrow(() =>
                    stream.TransactionalHandler<object>(
                        e =>
                        {
                            predicateCount++;
                            throw new InvalidOperationException("predicate1"); 
                        },
                        e => { handlerCount++; }));
                Assert.DoesNotThrow(() =>
                    stream.TransactionalHandler<object>(
                        e =>
                        {
                            predicateCount++;
                            throw new InvalidOperationException("predicate2");
                        },
                        e => { handlerCount++; }));

                Assert.Throws<InvalidOperationException>(() => stream.Raise(new object()));
                Assert.Equal(1, predicateCount);
                Assert.Equal(0, handlerCount);
                Assert.Equal(TransactionStatus.Aborted, Transaction.Current.TransactionInformation.Status);
            }
        }

        [Fact]
        public void TransactionalHandler_PassedPredicateAndAction_FiltersEventsUsingThePredicate()
        {
            var stream = new AppEventStream(e => { });
            var handledEvents = new List<SampleNonCriticalEvent>();
            stream.TransactionalHandler<SampleNonCriticalEvent>(e=>e.Value < 10, handledEvents.Add);

            stream.Raise(new SampleNonCriticalEvent { Value = 100 }); // Shouldn't be handled
            stream.Raise(new SampleNonCriticalEvent { Value = 1 }); // Should be handled

            Assert.Equal(1, handledEvents.Count);
            Assert.Equal(1, handledEvents[0].Value);
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenNoHandlersAndConcreteClass_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<SampleNonCriticalEvent>(e => { });
            Assert.True(stream.CriticalEventHasNoHandlers<SampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenConcreteClassHandler_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<SampleCriticalEvent>(e => { });
            Assert.False(stream.CriticalEventHasNoHandlers<SampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenIndependentConcreteClassHandlerOnly_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });
            stream.IndependentHandler<SampleCriticalEvent>(e => { });
            Assert.True(stream.CriticalEventHasNoHandlers<SampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenInterfaceHandler_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });

            // Handle ISampleCriticalEvent which is implemented by SampleCriticalEvent
            stream.TransactionalHandler<ISampleCriticalEvent>(e => { }); 
            Assert.False(stream.CriticalEventHasNoHandlers<ISampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenInterfaceHandlerAndClassParameter_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });

            // Handle ISampleCriticalEvent which is implemented by SampleCriticalEvent
            stream.TransactionalHandler<ISampleCriticalEvent>(e => { }); 
            Assert.True(stream.CriticalEventHasNoHandlers<SampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenConcreteClassHandlerAndInterfaceParameter_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });

            // Handle SampleCriticalEvent which is implements by SampleCriticalEvent
            stream.TransactionalHandler<SampleCriticalEvent>(e => { }); 
            Assert.False(stream.CriticalEventHasNoHandlers<ISampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenDerivedInterfaceHandler_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });

            // Handle ISampleCriticalEvent which is inherited by ISampleCriticalEvent
            stream.TransactionalHandler<ISampleCriticalEvent>(e => { }); 
            Assert.True(stream.CriticalEventHasNoHandlers<ISampleDerivedCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenDerivedConcreteClassHandler_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });

            // Handle SampleCriticalEvent which is inherited by SampleCriticalEvent
            stream.TransactionalHandler<SampleCriticalEvent>(e => { });
            Assert.True(stream.CriticalEventHasNoHandlers<SampleDerivedCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenInheritedInterfaceHandler_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });

            // Handle ISampleDerivedCriticalEvent which inherits ISampleCriticalEvent
            stream.TransactionalHandler<ISampleDerivedCriticalEvent>(e => { });
            Assert.False(stream.CriticalEventHasNoHandlers<ISampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenInheritedConcreteClassHandler_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });

            // Handle SampleDerivedCriticalEvent which inherits SampleCriticalEvent
            stream.TransactionalHandler<SampleDerivedCriticalEvent>(e => { });
            Assert.False(stream.CriticalEventHasNoHandlers<SampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenInheritedConcreteClassHandlerWithInterfaceParamter_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });

            // Handle SampleDerivedCriticalEvent which inherits ISampleCriticalEvent
            stream.TransactionalHandler<SampleDerivedCriticalEvent>(e => { });
            Assert.False(stream.CriticalEventHasNoHandlers<ISampleCriticalEvent>());
        }

        [Fact]
        public void CriticalEventHasNoHandlers_GivenInheritedInterfaceHandlerWithConcreteClassParamter_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });

            // Handle ISampleDerivedCriticalEvent which is implemented by ISampleCriticalEvent
            stream.TransactionalHandler<ISampleDerivedCriticalEvent>(e => { });
            Assert.True(stream.CriticalEventHasNoHandlers<SampleCriticalEvent>());
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        private class SampleDerivedCriticalEvent : SampleCriticalEvent, ISampleDerivedCriticalEvent { }


        [Fact]
        public void Raise_GivenMatchingTxHandlers_HandlersAreInvoked()
        {
            var work = new List<string>();
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<IBaseEvent>(evt => work.Add("1"));
            stream.TransactionalHandler<IDerivedEvent>(evt => work.Add("2"));
            stream.TransactionalHandler<ISecondEvent>(evt => work.Add("3"));
            stream.TransactionalHandler<IThirdEvent>(evt => work.Add("4"));
            stream.TransactionalHandler<IFourthEvent>(evt => work.Add("5"));
            stream.TransactionalHandler<BaseEventWithoutInterface>(evt => work.Add("6"));
            stream.TransactionalHandler<DerivedEventWithDerivedInterface>(evt => work.Add("7"));
            stream.TransactionalHandler<SuperDerivedEventWithTwoMoreInterfaces>(evt => work.Add("8"));
            stream.TransactionalHandler<object>(evt => work.Add("9"));

            stream.IndependentHandler<IBaseEvent>(evt => work.Add("a"));
            stream.IndependentHandler<IDerivedEvent>(evt => work.Add("b"));
            stream.IndependentHandler<ISecondEvent>(evt => work.Add("c"));
            stream.IndependentHandler<IThirdEvent>(evt => work.Add("d"));
            stream.IndependentHandler<IFourthEvent>(evt => work.Add("e"));
            stream.IndependentHandler<BaseEventWithoutInterface>(evt => work.Add("f"));
            stream.IndependentHandler<DerivedEventWithDerivedInterface>(evt => work.Add("g"));
            stream.IndependentHandler<SuperDerivedEventWithTwoMoreInterfaces>(evt => work.Add("h"));
            stream.IndependentHandler<object>(evt => work.Add("i"));

            Assert.DoesNotThrow(() => stream.Raise(new SuperDerivedEventWithTwoMoreInterfaces()));

            work.Sort(StringComparer.InvariantCulture);
            var result = new StringBuilder();
            work.ForEach(w=>result.Append(w));

            Assert.Equal("123456789abcdefghi", result.ToString());
        }


        [Fact]
        public void Raise_GivenNonMatchingTxHandlers_OnlyMatchingHandlersAreInvoked()
        {
            var work = new List<string>();
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<IBaseEvent>(evt => work.Add("1"));
            stream.TransactionalHandler<IDerivedEvent>(evt => work.Add("2"));
            stream.TransactionalHandler<ISecondEvent>(evt => work.Add("3"));
            stream.TransactionalHandler<IThirdEvent>(evt => work.Add("4"));
            stream.TransactionalHandler<IFourthEvent>(evt => work.Add("5"));
            stream.TransactionalHandler<BaseEventWithoutInterface>(evt => work.Add("6"));
            stream.TransactionalHandler<DerivedEventWithDerivedInterface>(evt => work.Add("7"));
            stream.TransactionalHandler<SuperDerivedEventWithTwoMoreInterfaces>(evt => work.Add("8"));
            stream.TransactionalHandler<object>(evt => work.Add("9"));

            stream.IndependentHandler<IBaseEvent>(evt => work.Add("a"));
            stream.IndependentHandler<IDerivedEvent>(evt => work.Add("b"));
            stream.IndependentHandler<ISecondEvent>(evt => work.Add("c"));
            stream.IndependentHandler<IThirdEvent>(evt => work.Add("d"));
            stream.IndependentHandler<IFourthEvent>(evt => work.Add("e"));
            stream.IndependentHandler<BaseEventWithoutInterface>(evt => work.Add("f"));
            stream.IndependentHandler<DerivedEventWithDerivedInterface>(evt => work.Add("g"));
            stream.IndependentHandler<SuperDerivedEventWithTwoMoreInterfaces>(evt => work.Add("h"));
            stream.IndependentHandler<object>(evt => work.Add("i"));

            Assert.DoesNotThrow(() => stream.Raise(new DerivedEventWithDerivedInterface()));

            work.Sort(StringComparer.InvariantCulture);
            var result = new StringBuilder();
            work.ForEach(w=>result.Append(w));

            Assert.Equal("12679abfgi", result.ToString());
        }

        [Fact]
        public void HasTransactionalHandler_GivenNullEvent_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });
            Assert.False(stream.HasTransactionalHandler(null));
        }

        [Fact]
        public void HasTransactionalHandler_GivenNoHandler_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });
            Assert.False(stream.HasTransactionalHandler(new object()));
        }

        [Fact]
        public void HasTransactionalHandler_GivenNonTransactionalHandlerOnly_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });
            stream.IndependentHandler<object>(e => { });
            Assert.False(stream.HasTransactionalHandler(new object()));
        }

        [Fact]
        public void HasTransactionalHandler_GivenTransactionalWithoutPredicate_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<object>(e => { });
            Assert.True(stream.HasTransactionalHandler(new object()));
        }

        [Fact]
        public void HasTransactionalHandler_GivenTransactionalWithSingleMatchingPredicate_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<object>(e => true, e => { });
            Assert.True(stream.HasTransactionalHandler(new object()));
        }

        [Fact]
        public void HasTransactionalHandler_GivenTransactionalWithMultipleMatchingPredicates_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<object>(e => true, e => { });
            stream.TransactionalHandler<object>(e => true, e => { });
            Assert.True(stream.HasTransactionalHandler(new object()));
        }

        [Fact]
        public void HasTransactionalHandler_GivenTransactionalWithSingleMatchingPredicateOfMany_ReturnsTrue()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<string>(e => e == "foo", e => { });
            stream.TransactionalHandler<string>(e => e == "bar", e => { });
            Assert.True(stream.HasTransactionalHandler("foo"));
            Assert.True(stream.HasTransactionalHandler("bar"));
            Assert.False(stream.HasTransactionalHandler("baz"));
        }

        [Fact]
        public void HasTransactionalHandler_GivenTransactionalWithNonMatchingPredicate_ReturnsFalse()
        {
            var stream = new AppEventStream(e => { });
            stream.TransactionalHandler<object>(e=>false, e => { });
            Assert.False(stream.HasTransactionalHandler(new object()));
        }
    }

    internal interface IBaseEvent { }
    internal interface IDerivedEvent : IBaseEvent { }
    internal interface ISecondEvent { }
    internal interface IThirdEvent { }
    internal interface IFourthEvent : IThirdEvent  { }
    internal class BaseEventWithoutInterface { }
    internal class DerivedEventWithDerivedInterface : BaseEventWithoutInterface, IDerivedEvent { }
    internal class SuperDerivedEventWithTwoMoreInterfaces : DerivedEventWithDerivedInterface, ISecondEvent, IFourthEvent { }

}
