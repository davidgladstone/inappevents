﻿using System;
using Xunit;

namespace InAppEvents.Tests
{
    public class EnsureTests
    {
        [Fact]
        public void ArgNotNull_GivenNullAndNoParamName_ThrowsArgumentNullExceptionWithNullParamName()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => Ensure.ParamIsNotNull(null));
            Assert.Null(ex.ParamName);
        }

        [Fact]
        public void ArgNotNull_GivenNullAndArgName_ThrowsArgumentNullExceptionWithNameSet()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => Ensure.ParamIsNotNull(null, "foo"));
            Assert.Equal("foo", ex.ParamName);
        }

        [Fact]
        public void ArgNotNull_GivenNotNull_DoesNotThrow()
        {
            Assert.DoesNotThrow(() => Ensure.ParamIsNotNull(new object()));
        }
    }
}
