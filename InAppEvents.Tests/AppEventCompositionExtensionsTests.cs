﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using InAppEvents.Tests.Support;
using Xunit;

namespace InAppEvents.Tests
{
    public class AppEventCompositionExtensionsTests
    {
        [Fact]
        public void LoadHandlers_GivenNullCompositionContainer_ThrowsArgumentNullException()
        {
            var appEvents = new AppEventStream(e => { });
            Assert.Throws<ArgumentNullException>(() => appEvents.LoadHandlers(null, h => { }));
        }

        [Fact]
        public void LoadHandlers_GivenCompositionHostWithHandlersAndNullBuildUpAction_SubscribesAllHandlersWithoutThrowing()
        {
            var appEvents = new TestAppEventStream();
            var catalog = new TypeCatalog(new [] {typeof(TestHandler1), typeof(TestHandler2)});
            var container = new CompositionContainer(catalog);
            Assert.DoesNotThrow(() => appEvents.LoadHandlers(container, null));
            Assert.Equal(2, appEvents.HandlersAdded.Count);
            Assert.True(appEvents.HandlersAdded.Any(h=>h is TestHandler1));
            Assert.True(appEvents.HandlersAdded.Any(h=>h is TestHandler2));
        }

        [Fact]
        public void LoadHandlers_GivenCompositionHostWithHandlersAndBuildUpAction_InvokesBuildUpActionAndSubscribesAllHandlers()
        {
            var appEvents = new TestAppEventStream();
            var catalog = new TypeCatalog(new [] {typeof(TestHandler1), typeof(TestHandler2)});
            var container = new CompositionContainer(catalog);
            var builtUp = new List<object>();
            
            Assert.DoesNotThrow(() => appEvents.LoadHandlers(container, builtUp.Add));
            
            Assert.Equal(2, builtUp.Count);
            Assert.True(builtUp.Any(h => h is TestHandler1));
            Assert.True(builtUp.Any(h => h is TestHandler2));

            Assert.Equal(2, appEvents.HandlersAdded.Count);
            Assert.True(appEvents.HandlersAdded.Any(h => h is TestHandler1));
            Assert.True(appEvents.HandlersAdded.Any(h => h is TestHandler2));
        }

        private class TestAppEventStream : IAppEventStream
        {
            public List<IAppEventHandler> HandlersAdded { get; private set; }

            public TestAppEventStream()
            {
                // Just record what was added for testing purposes.
                HandlersAdded = new List<IAppEventHandler>();
            }

            public void AddHandlers(params IAppEventHandler[] handlers)
            {
                HandlersAdded.AddRange(handlers);
            }

            public void Raise(object @event)
            {
                throw new NotImplementedException();
            }

            public void TransactionalHandler<TEvent>(Action<TEvent> action) where TEvent : class
            {
                throw new NotImplementedException();
            }

            public void TransactionalHandler<TEvent>(Func<TEvent, bool> predicate, Action<TEvent> action) where TEvent : class
            {
                throw new NotImplementedException();
            }

            public void IndependentHandler<TEvent>(Action<TEvent> action) where TEvent : class
            {
                throw new NotImplementedException();
            }

            public void IndependentHandler<TEvent>(Func<TEvent, bool> predicate, Action<TEvent> action) where TEvent : class
            {
                throw new NotImplementedException();
            }

            public bool CriticalEventHasNoHandlers<TEvent>() where TEvent : class, ICriticalEvent
            {
                throw new NotImplementedException();
            }

            public bool HasTransactionalHandler(object @event)
            {
                throw new NotImplementedException();
            }
        }
    }

    [Export(typeof(IAppEventHandler))]
    internal class TestHandler1 : TestAppEventHandler { }
    [Export(typeof(IAppEventHandler))]
    internal class TestHandler2 : TestAppEventHandler { }
}
