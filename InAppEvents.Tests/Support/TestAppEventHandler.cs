using System;
using System.ComponentModel.Composition;

namespace InAppEvents.Tests.Support
{
    [Export(typeof (IAppEventHandler))]
    class TestAppEventHandler : IAppEventHandler
    {
        public bool IsSubscribed { get; private set; }

        public void Subscribe(IAppEventStream events)
        {
            if (IsSubscribed)
            {
                throw new InvalidOperationException("Subscribe called more than once.");
            }
            IsSubscribed = true;
        }
    }
}