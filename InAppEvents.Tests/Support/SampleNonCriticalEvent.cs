namespace InAppEvents.Tests.Support
{
    public class SampleNonCriticalEvent
    {
        public void MarkAsHandled()
        {
            WasHandled = true;
        }

        public bool WasHandled { get; private set; }

        public int Value { get; set; }
    }
}