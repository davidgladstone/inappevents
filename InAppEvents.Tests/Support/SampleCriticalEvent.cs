namespace InAppEvents.Tests.Support
{
    public class SampleCriticalEvent : ISampleCriticalEvent
    {
        public void MarkAsHandled()
        {
            WasHandled = true;
        }

        public bool WasHandled { get; private set; }
    }
}