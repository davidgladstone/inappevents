using System;

namespace InAppEvents
{
    /// <summary>
    /// Defines the operations available on an AppEventStream for mocking and testing purposes.
    /// </summary>
    public interface IAppEventStream
    {
        /// <summary>
        /// Calls the <see cref="IAppEventHandler.Subscribe"/> method passing this
        /// object as the parameter to allow the objects to set up their handlers.
        /// Subscribe will not be called on any handler that has previously been
        /// added to this event stream.
        /// </summary>
        /// <param name="handlers">The handlers to add.</param>
        /// <exception cref="ArgumentNullException"><paramref name="handlers"/> is null.</exception>
        /// <exception cref="ArgumentException">A null handler was provided.</exception>
        void AddHandlers(params IAppEventHandler[] handlers);

        /// <summary>
        /// Raise an event for immediate handling by previously-registered
        /// transactional and independent event handlers. If the object describing
        /// the event implements <see cref="ICriticalEvent"/> then a transactional 
        /// handler must invoke <see cref="ICriticalEvent.MarkAsHandled"/> or else
        /// a <see cref="CriticalEventHandlingException"/> will be raised. This 
        /// behaviour is to protect against misconfiguration resulting in a critical
        /// event from not being handled.
        /// </summary>
        /// 
        /// <param name="event">Any object describing an event. Typically this
        /// will be a simple data object without behaviour.</param>
        /// 
        /// <exception cref="CriticalEventHandlingException">The parameter 
        /// <paramref name="event"/> implements <see cref="ICriticalEvent"/> and 
        /// was already marked as complete before being passed to this method or
        /// (more likely) no transactional handler invoked 
        /// <see cref="ICriticalEvent.MarkAsHandled"/>.</exception>
        /// 
        /// <exception cref="ArgumentNullException"><paramref name="event"/> was
        /// null.</exception>
        void Raise(object @event);

        /// <summary>
        /// Registers an action as a transactional handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>). The handler will be invoked on the thread
        /// that raises the event by calling <see cref="Raise"/>. 
        /// 
        /// The action should modify the event since order of handler execution is not 
        /// guaranteed and this could result in unpredictable results.
        /// 
        /// Execution will take place within a transaction shared by all other transactional 
        /// handlers for this one event. If an ambient transaction exists then it will be 
        /// used otherwise a new transaction will be created.
        /// 
        /// If any transactional handler throws an unhandled exception during execution then
        /// the transaction will be aborted and subsequent transactional handlers will not
        /// execute for this event. Independent handlers will still be executed however.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        void TransactionalHandler<TEvent>(Action<TEvent> action) where TEvent : class;

        /// <summary>
        /// Registers an action as a transactional handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>) that meets the requirements of the specified
        /// predicate. The handler will be invoked on the thread that raises the event by 
        /// calling <see cref="Raise"/>.
        /// 
        /// Neither the action nor the predicate should modify the event since order
        /// of handler execution is not guaranteed and this could result in unpredictable
        /// results.
        /// 
        /// Execution will take place within a transaction shared by all other transactional 
        /// handlers for this one event. If an ambient transaction exists then it will be 
        /// used otherwise a new transaction will be created.
        /// 
        /// If any transactional handler throws an unhandled exception during execution then
        /// the transaction will be aborted and subsequent transactional handlers will not
        /// execute for this event. Independent handlers will still be executed however.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="predicate">A function that determines whether an event should be
        /// handled by <paramref name="action"/>.</param>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="predicate"/> is null.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        void TransactionalHandler<TEvent>(Func<TEvent, bool> predicate, Action<TEvent> action) where TEvent : class;

        /// <summary>
        /// Registers an action as an independent handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>). The handler will be invoked on the thread that 
        /// raises the event by calling <see cref="Raise"/>. 
        /// 
        /// The action should modify the event since order of handler execution is not 
        /// guaranteed and this could result in unpredictable results.
        /// 
        /// Execution will take place within a suppressed transaction scope (i.e. no transaction).
        /// 
        /// If any independent handler throws an unhandled exception during execution then
        /// the exception will be passed to the exception handler defined in the constructor
        /// and other independent handlers will continue to execute for the event. Independent 
        /// handlers are invoked after transactional handlers.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        void IndependentHandler<TEvent>(Action<TEvent> action) where TEvent : class;

        /// <summary>
        /// Registers an action as an independent handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>) that meets the requirements of the specified
        /// predicate. The handler will be invoked on the thread that raises the event by 
        /// calling <see cref="Raise"/>. 
        /// 
        /// Neither the action nor the predicate should modify the event since order
        /// of handler execution is not guaranteed and this could result in unpredictable
        /// results.
        /// 
        /// Execution will take place within a suppressed transaction scope (i.e. no transaction).
        /// 
        /// If any independent handler throws an unhandled exception during execution then
        /// the exception will be passed to the exception handler defined in the constructor
        /// and other independent handlers will continue to execute for the event. Independent 
        /// handlers are invoked after transactional handlers.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="predicate">A function that determines whether an event should be
        /// handled by <paramref name="action"/>.</param>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="predicate"/> is null.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        void IndependentHandler<TEvent>(Func<TEvent, bool> predicate, Action<TEvent> action) where TEvent : class;


        /// <summary>
        /// Returns true if there are no transactional handlers registered for the critical event
        /// type <typeparamref name="TEvent"/>; otherwise, false. Note that just because a handler
        /// is registered, it does not mean that it will always run since it may use a predicate
        /// to filter events. Think of this as a start-up canary-in-the-coalmine indicating that
        /// a critical event will definitely not be handled.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to test.</typeparam>
        bool CriticalEventHasNoHandlers<TEvent>() where TEvent : class, ICriticalEvent;


        /// <summary>
        /// Returns true if the @event has a transactional event handler; otherwise, false.
        /// </summary>
        /// <param name="event">The event to evaluate.</param>
        bool HasTransactionalHandler(object @event);

    }
}