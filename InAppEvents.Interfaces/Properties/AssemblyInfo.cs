﻿using System.Reflection;

[assembly: AssemblyTitle("InAppEvents.Interfaces")]
[assembly: AssemblyDescription("Key interfaces for use by handler components.")]