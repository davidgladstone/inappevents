namespace InAppEvents
{
    /// <summary>
    /// Implement this interface in your events to get a guarantee that
    /// a transactional event handler has handled your event. This is to 
    /// guard against mis-configuration resulting in an event that must
    /// be handled being dropped.
    /// 
    /// Critical event handlers must invoke the <see cref="MarkAsHandled"/>
    /// method during processing to avoid <see cref="IAppEventStream.Raise"/>
    /// from throwing <see cref="CriticalEventHandlingException"/>.
    /// </summary>
    public interface ICriticalEvent
    {
        /// <summary>
        /// When implemented should set <see cref="WasHandled"/> to true. 
        /// Critical event handlers must invoke the <see cref="MarkAsHandled"/>
        /// method during processing to avoid <see cref="IAppEventStream.Raise"/>
        /// from throwing <see cref="CriticalEventHandlingException"/>.
        /// </summary>
        void MarkAsHandled();

        /// <summary>
        /// Gets an value indicating whether or not the critical event
        /// was handled.
        /// </summary>
        bool WasHandled { get; }
    }
}