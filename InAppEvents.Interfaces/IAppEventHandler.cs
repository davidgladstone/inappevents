﻿namespace InAppEvents
{
    /// <summary>
    /// Represents a class that contains one or more event handlers that
    /// can be subscribed to an instance of <see cref="IAppEventStream"/>.
    /// </summary>
    public interface IAppEventHandler
    {
        /// <summary>
        /// When implemented this method should add handlers to the event stream
        /// provided.
        /// </summary>
        /// <param name="events">The event stream that may be subscribed to.</param>
        void Subscribe(IAppEventStream events);
    }
}