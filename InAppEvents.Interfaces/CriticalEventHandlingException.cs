using System;

namespace InAppEvents
{
    /// <summary>
    /// Raised by <see cref="IAppEventStream.Raise"/> when an event implementing 
    /// <see cref="ICriticalEvent"/> was not handled correctly.
    /// </summary>
    public class CriticalEventHandlingException : Exception
    {
        /// <summary>
        /// Creates a new instance of the exception.
        /// </summary>
        /// <param name="message">The message associated with the exception.</param>
        public CriticalEventHandlingException(string message) : base(message)
        {
        }
    }
}