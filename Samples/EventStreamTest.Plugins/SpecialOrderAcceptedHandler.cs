﻿using System;
using System.ComponentModel.Composition;
using InAppEvents;
using EventStreamTest.Interfaces;

namespace EventStreamTest.Plugins
{
    [Export(typeof(IAppEventHandler))]
    public class SpecialOrderAcceptedHandler : IAppEventHandler
    {
        public void Subscribe(IAppEventStream events)
        {
            events.TransactionalHandler<ISpecialOrderAccepted>(e => e.Id < 100, HandleSpecialOrder);
            events.IndependentHandler<ISpecialOrderAccepted>(LogSpecialOrder);
        }

        private void HandleSpecialOrder(ISpecialOrderAccepted order)
        {
            Console.WriteLine("Special Order (handled) {0}-{1}", order.Id, order.Foo);
            order.MarkAsHandled();
        }

        private void LogSpecialOrder(ISpecialOrderAccepted order)
        {
            Console.WriteLine("Special Order (logged) {0}-{1}", order.Id, order.Foo);
        }
    }
}