﻿using System;
using System.ComponentModel.Composition;
using InAppEvents;
using EventStreamTest.Interfaces;

namespace EventStreamTest.Plugins
{
    [Export(typeof(IAppEventHandler))]
    public class OrderAcceptedHandler : IAppEventHandler
    {
        public void Subscribe(IAppEventStream events)
        {
            events.TransactionalHandler<IOrderAccepted>(evt => Console.WriteLine("Order {0} handled", evt.Id));
        }
    }
}