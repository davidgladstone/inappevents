﻿using System;
using InAppEvents;

namespace EventStreamTest
{
    public class LoggerAndValidator : IAppEventHandler
    {
        public void Subscribe(IAppEventStream events)
        {
            events.IndependentHandler<object>(e => Console.WriteLine("Logger: " + e.GetType().ToString()));
            events.IndependentHandler<OrderAccepted>(o=>o.Id <= 0, o => { throw new Exception("Bad Id: " + o.Id); });
        }
    }
}