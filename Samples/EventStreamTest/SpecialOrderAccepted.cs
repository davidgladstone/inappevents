﻿using EventStreamTest.Interfaces;

namespace EventStreamTest
{
    public class SpecialOrderAccepted : OrderAccepted, ISpecialOrderAccepted
    {
        public string Foo { get; set; }

        public void MarkAsHandled()
        {
            WasHandled = true;
        }

        public bool WasHandled { get; private set; }
    }
}