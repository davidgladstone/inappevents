﻿using EventStreamTest.Interfaces;

namespace EventStreamTest
{
    public class OrderAccepted : IOrderAccepted
    {
        public int Id { get; set; }
    }
}