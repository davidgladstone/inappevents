﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using InAppEvents;

namespace EventStreamTest
{
    class Program
    {
        static void Main()
        {
            var events = new AppEventStream(LogIndependentHandlerExceptions);

            CreateEventHandlers(events);

            Console.WriteLine("\n[Raising Order Accepted Event]");
            events.Raise(new OrderAccepted { Id = 1 });

            Console.WriteLine("\n[Raising SpecialOrder Accepted Event with ID 2 - expect both OrderAccepted and SpecialOrderAccepted]");
            events.Raise(new SpecialOrderAccepted { Id = 2, Foo = "Woot!" });

            Console.WriteLine("\n[Raising SpecialOrder Accepted Event with bad ID - expect tx handlers per 2 but non-tx to fail with bad ID message]");
            events.Raise(new SpecialOrderAccepted());

            try
            {
                Console.WriteLine("\n[Raising SpecialOrder Accepted Event high ID - expect message not handled exception]");
                events.Raise(new SpecialOrderAccepted {Id = 1000, Foo = "Should fail to process."});
            }
            catch (Exception e)
            {
                Console.WriteLine("*** Caught exception in transactional handler: {0}", e.Message);
            }
            Console.WriteLine("\n[Raising SpecialOrder Accepted Event with ID 4 - expect events to be handled as per ID 2]");
            events.Raise(new SpecialOrderAccepted { Id = 3, Foo = "Woot again!" });

            Console.WriteLine("\nDone. Press enter to quit.");
            Console.ReadLine();
        }

        private static void LogIndependentHandlerExceptions(Exception e)
        {
            Console.WriteLine(">>> Exception in independent handler: {0}", e.Message);
        }

        private static void CreateEventHandlers(IAppEventStream events)
        {
            // And some handlers the straightforward way.
            events.AddHandlers(new LoggerAndValidator());

            // Use MEF to discover handlers in another directory.
            var pluginPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), @"..\..\..\EventStreamTest.Plugins\bin\Debug");
            var catalog = new DirectoryCatalog(pluginPath, "*.dll");
            events.LoadHandlers(new CompositionContainer(catalog), null);
        }
    }
}
