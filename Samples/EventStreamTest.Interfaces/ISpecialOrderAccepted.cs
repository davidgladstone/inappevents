﻿using InAppEvents;

namespace EventStreamTest.Interfaces
{
    public interface ISpecialOrderAccepted : ICriticalEvent, IOrderAccepted
    {
        string Foo { get; }
    }
}