﻿namespace EventStreamTest.Interfaces
{
    public interface IOrderAccepted
    {
        int Id { get; }
    }
}