# README #

Version 0.2.1-alpha *UNSTABLE*

### About InAppEvents ###

InAppEvents is a collection of .NET assemblies designed to help developers create loosely coupled applications by introducing an in-memory event bus with support for transactional and non-transactional handlers. Handlers can be loaded at runtime like "plug-ins"  meaning that the core application solution does not require static references and can be extended at deployment time without requiring re-compilation.

### Current State ###

InAppEvents is currently proof-of-concept with the intention that it will be used within the context of ASP.NET MVC action methods. As it is used in anger the API will no doubt change.

### Questions or comments? ###

Contact: *david.w.gladstone@gmail.com*

