SET version=%1
if not exist packages md packages
..\.nuget\nuget.exe pack ..\InAppEvents.Interfaces\InAppEvents.Interfaces.nuspec -Version %version% -Output packages
..\.nuget\nuget.exe pack ..\InAppEvents.Core\InAppEvents.Core.nuspec -Version %version% -Output packages
..\.nuget\nuget.exe pack ..\InAppEvents.Composition\InAppEvents.Composition.nuspec -Version %version% -Output packages
pause