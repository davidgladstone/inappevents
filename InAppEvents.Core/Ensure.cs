using System;

namespace InAppEvents
{
    /// <summary>
    /// An internal class with helper validation functions for pre- and post-conditions.
    /// </summary>
    public static class Ensure
    {
        /// <summary>
        /// Throws <see cref="ArgumentNullException"/> if <paramref name="value"/> is null.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <param name="paramName">The name of the parameter being tested.</param>
        public static void ParamIsNotNull(object value, string paramName = null)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName);
            }
        }
    }
}