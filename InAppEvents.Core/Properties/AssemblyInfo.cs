﻿using System.Reflection;

[assembly: AssemblyTitle("InAppEvents.Core")]
[assembly: AssemblyDescription("Core implementation of the event bus.")]