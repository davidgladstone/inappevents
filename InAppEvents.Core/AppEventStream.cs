using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Transactions;

namespace InAppEvents
{
    /// <summary>
    /// Provides a mechanism for raising and handling events in a synchronous
    /// fashion. Handlers can be either independent or transactional with the 
    /// primary difference being how exceptions are handled.
    /// </summary>
    public class AppEventStream : IAppEventStream
    {
        private readonly Action<Exception> _onIndependentHandlerException;
        private readonly ISubject<object> _eventsForTransactionalHandlers = new Subject<object>();
        private readonly ISubject<object> _eventsForIndependentHandlers = new Subject<object>();
        private ImmutableHashSet<IAppEventHandler> _handlers;
        private ImmutableDictionary<Type, Func<object, bool>> _handledTypes = ImmutableDictionary<Type, Func<object, bool>>.Empty;
        private readonly object _gate = new object();
        private readonly TransactionOptions _newTransactionOptions;

        /// <summary>
        /// Constructs a new instance of the class with instructions for how
        /// to deal with exceptions that occur in independent handlers.
        /// </summary>
        /// <param name="onIndependentHandlerException">An action that will
        /// be invoked whenever an exception arises during the execution of
        /// an independent event handler.</param>
        /// <param name="newTransactionOptions">The options to use when a transaction scope
        /// needs to be created when calling <see cref="Raise"/> without an ambient transaction.</param>
        /// <exception cref="ArgumentNullException">The exception handling action provided was null.</exception>
        public AppEventStream(Action<Exception> onIndependentHandlerException, TransactionOptions newTransactionOptions)
        {
            Ensure.ParamIsNotNull(onIndependentHandlerException);
            _onIndependentHandlerException = onIndependentHandlerException;
            _handlers = ImmutableHashSet<IAppEventHandler>.Empty;
            _newTransactionOptions = newTransactionOptions;
        }

        /// <summary>
        /// Constructs a new instance of the class with instructions for how to deal with 
        /// exceptions that occur in independent handlers. If this constructor is used
        /// and <see cref="Raise"/> is invoked without an ambient transaction then 
        /// a transaction with <see cref="IsolationLevel.RepeatableRead"/> isolation and a 
        /// timeout of <see cref="TransactionManager.MaximumTimeout"/>.
        /// </summary>
        /// <param name="onIndependentHandlerException">An action that will
        /// be invoked whenever an exception arises during the execution of
        /// an independent event handler.</param>
        /// <exception cref="ArgumentNullException">The exception handling
        /// action provided was null.</exception>
        public AppEventStream(Action<Exception> onIndependentHandlerException) : this(
            onIndependentHandlerException, 
            new TransactionOptions
            {
                IsolationLevel = IsolationLevel.RepeatableRead,
                Timeout = TransactionManager.MaximumTimeout
            })
        {
        }


        /// <summary>
        /// Raise an event for immediate handling by previously-registered
        /// transactional and independent event handlers. If the object describing
        /// the event implements <see cref="ICriticalEvent"/> then a transactional 
        /// handler must invoke <see cref="ICriticalEvent.MarkAsHandled"/> or else
        /// a <see cref="CriticalEventHandlingException"/> will be raised. This 
        /// behaviour is to protect against misconfiguration resulting in a critical
        /// event from not being handled.
        /// </summary>
        /// 
        /// <param name="event">Any object describing an event. Typically this
        /// will be a simple data object without behaviour.</param>
        /// 
        /// <exception cref="CriticalEventHandlingException">The parameter 
        /// <paramref name="event"/> implements <see cref="ICriticalEvent"/> and 
        /// was already marked as complete before being passed to this method or
        /// (more likely) no transactional handler invoked 
        /// <see cref="ICriticalEvent.MarkAsHandled"/>.</exception>
        /// 
        /// <exception cref="ArgumentNullException"><paramref name="event"/> was
        /// null.</exception>
        public void Raise(object @event)
        {
            if (@event == null) throw new ArgumentNullException("event");

            AssertHandledState(@event, false, "Event should not be marked as handled before handlers are run.");

            try
            {
                bool hasTransactionalHandler;
                try
                {
                    hasTransactionalHandler = HasTransactionalHandler(@event);
                }
                catch
                {
                    // Ensure that any outer transaction is aborted by enlisting in it before throwing.
                    using (new TransactionScope(TransactionScopeOption.Required, _newTransactionOptions))
                    {
                        throw;
                    }
                }

                // Only create a transaction scope if we have a transactional handler.
                if (hasTransactionalHandler)
                {
                    using (var ts = new TransactionScope(TransactionScopeOption.Required, _newTransactionOptions))
                    {
                        _eventsForTransactionalHandlers.OnNext(@event);
                        AssertHandledState(@event, true, "Critical event was not handled by a transactional handler.");
                        ts.Complete();
                    }
                }
                else
                {
                    AssertHandledState(@event, true, "Critical event was not handled by a transactional handler.");
                }
            }
            finally
            {
                using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    _eventsForIndependentHandlers.OnNext(@event);
                    ts.Complete();
                }
            }
        }

        // ReSharper disable once UnusedParameter.Local
        private static void AssertHandledState(object @event, bool expectedState, string message)
        {
            var criticalEvent = @event as ICriticalEvent;
            if (criticalEvent != null && criticalEvent.WasHandled != expectedState)
            {
                throw new CriticalEventHandlingException(message);
            }
        }


        /// <summary>
        /// Registers an action as a transactional handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>). The handler will be invoked on the thread
        /// that raises the event by calling <see cref="Raise"/>. 
        /// 
        /// The action should modify the event since order of handler execution is not 
        /// guaranteed and this could result in unpredictable results.
        /// 
        /// Execution will take place within a transaction shared by all other transactional 
        /// handlers for this one event. If an ambient transaction exists then it will be 
        /// used otherwise a new transaction will be created.
        /// 
        /// If any transactional handler throws an unhandled exception during execution then
        /// the transaction will be aborted and subsequent transactional handlers will not
        /// execute for this event. Independent handlers will still be executed however.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        public void TransactionalHandler<TEvent>(Action<TEvent> action) where TEvent : class
        {
            Ensure.ParamIsNotNull(action);

            _eventsForTransactionalHandlers
                .Where(evt => evt is TEvent)
                .Select(evt => (TEvent)evt)
                .SubscribeOn(Scheduler.Immediate)
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(action);

            RegisterHandledType<TEvent>();
        }

        /// <summary>
        /// Registers an action as a transactional handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>) that meets the requirements of the specified
        /// predicate. The handler will be invoked on the thread that raises the event by 
        /// calling <see cref="Raise"/>. 
        /// 
        /// Neither the action nor the predicate should modify the event since order
        /// of handler execution is not guaranteed and this could result in unpredictable
        /// results.
        /// 
        /// Execution will take place within a transaction shared by all other transactional 
        /// handlers for this one event. If an ambient transaction exists then it will be 
        /// used otherwise a new transaction will be created.
        /// 
        /// If any transactional handler throws an unhandled exception during execution then
        /// the transaction will be aborted and subsequent transactional handlers will not
        /// execute for this event. Independent handlers will still be executed however.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="predicate">A function that determines whether an event should be
        /// handled by <paramref name="action"/>.</param>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="predicate"/> is null.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        public void TransactionalHandler<TEvent>(Func<TEvent, bool> predicate, Action<TEvent> action) where TEvent : class
        {
            Ensure.ParamIsNotNull(predicate);
            Ensure.ParamIsNotNull(action);

            _eventsForTransactionalHandlers
                .Where(evt => evt is TEvent && (predicate((TEvent) evt)))
                .Select(evt => (TEvent)evt)
                .SubscribeOn(Scheduler.Immediate)
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(action);

            RegisterHandledType(predicate);
        }

        private void RegisterHandledType<TEvent>() where TEvent : class
        {
            RegisterHandledType<TEvent>(o=>true);
        }

        private void RegisterHandledType<TEvent>(Func<TEvent, bool> predicate) where TEvent : class
        {
            lock (_gate)
            {
                Func<object, bool> existingPredicate;
                _handledTypes = _handledTypes.TryGetValue(typeof (TEvent), out existingPredicate) 
                    ? _handledTypes.SetItem(typeof(TEvent), o => existingPredicate(o) || predicate((TEvent)o)) 
                    : _handledTypes.Add(typeof(TEvent), o => predicate((TEvent) o));
            }
        }

        /// <summary>
        /// Registers an action as an independent handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>). The handler will be invoked on the thread that 
        /// raises the event by calling <see cref="Raise"/>. 
        /// 
        /// The action should modify the event since order of handler execution is not 
        /// guaranteed and this could result in unpredictable results.
        /// 
        /// Execution will take place within a suppressed transaction scope (i.e. no transaction).
        /// 
        /// If any independent handler throws an unhandled exception during execution then
        /// the exception will be passed to the exception handler defined in the constructor
        /// and other independent handlers will continue to execute for the event. Independent 
        /// handlers are invoked after transactional handlers.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        public void IndependentHandler<TEvent>(Action<TEvent> action) where TEvent : class
        {
            Ensure.ParamIsNotNull(action);

            _eventsForIndependentHandlers
                .Where(evt => evt is TEvent)
                .Select(evt => (TEvent) evt)
                .SubscribeOn(Scheduler.Immediate)
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(evt =>
                {
                    try
                    {
                        action(evt);
                    }
                    catch (Exception e)
                    {
                        _onIndependentHandlerException(e);
                    }
                });
        }

        /// <summary>
        /// Registers an action as an independent handler for a particular type of event 
        /// (<typeparamref name="TEvent"/>) that meets the requirements of the specified
        /// predicate. The handler will be invoked on the thread that raises the event by 
        /// calling <see cref="Raise"/>. 
        /// 
        /// Neither the action nor the predicate should modify the event since order
        /// of handler execution is not guaranteed and this could result in unpredictable
        /// results.
        /// 
        /// Execution will take place within a suppressed transaction scope (i.e. no transaction).
        /// 
        /// If any independent handler throws an unhandled exception during execution then
        /// the exception will be passed to the exception handler defined in the constructor
        /// and other independent handlers will continue to execute for the event. Independent 
        /// handlers are invoked after transactional handlers.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to handle. This can be a class 
        /// or an interface.</typeparam>
        /// <param name="predicate">A function that determines whether an event should be
        /// handled by <paramref name="action"/>.</param>
        /// <param name="action">The action to perform when an event of type 
        /// <typeparamref name="TEvent"/> is raised.</param>
        /// <exception cref="ArgumentNullException"><paramref name="predicate"/> is null.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="action"/> is null.</exception>
        public void IndependentHandler<TEvent>(Func<TEvent, bool> predicate, Action<TEvent> action) where TEvent : class
        {
            Ensure.ParamIsNotNull(predicate);
            Ensure.ParamIsNotNull(action);

            _eventsForIndependentHandlers
                .Where(evt =>
                {
                    try
                    {
                        return evt is TEvent && (predicate((TEvent) evt));
                    }
                    catch (Exception e)
                    {
                        _onIndependentHandlerException(e);
                        return false;
                    }
                })
                .Select(evt => (TEvent) evt)
                .SubscribeOn(Scheduler.Immediate)
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(evt =>
                {
                    try
                    {
                        action(evt);
                    }
                    catch (Exception e)
                    {
                        _onIndependentHandlerException(e);
                    }
                });
        }

        /// <summary>
        /// Calls the <see cref="IAppEventHandler.Subscribe"/> method passing this
        /// object as the parameter to allow the objects to set up their handlers.
        /// Subscribe will not be called on any handler that has previously been
        /// added to this event stream.
        /// </summary>
        /// <param name="handlers">The handlers to add.</param>
        /// <exception cref="ArgumentNullException"><paramref name="handlers"/> is null.</exception>
        /// <exception cref="ArgumentException">A null handler was provided.</exception>
        public void AddHandlers(params IAppEventHandler[] handlers)
        {
            lock (_gate)
            {
                Ensure.ParamIsNotNull(handlers);
                if (handlers.Any(h => h == null))
                {
                    throw new ArgumentException("An attempt was made to add a null handler");
                }

                var notYetSubscribed = handlers.Except(_handlers).ToImmutableHashSet();
                _handlers = _handlers.Union(handlers);
                foreach (var handler in notYetSubscribed)
                {
                    handler.Subscribe(this);
                }
            }
        }


        /// <summary>
        /// Returns true if there are no transactional handlers registered for the critical event
        /// type <typeparamref name="TEvent"/>; otherwise, false. Note that just because a handler
        /// is registered, it does not mean that it will always run since it may use a predicate
        /// to filter events. Think of this as a start-up canary-in-the-coalmine indicating that
        /// a critical event will definitely not be handled.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to test.</typeparam>
        public bool CriticalEventHasNoHandlers<TEvent>() where TEvent : class, ICriticalEvent
        {
            return !_handledTypes.Any(t => typeof(TEvent).IsAssignableFrom(t.Key));
        }


        /// <summary>
        /// Returns true if the @event has a transactional event handler; otherwise, false.
        /// </summary>
        /// <param name="event">The event to evaluate.</param>
        public bool HasTransactionalHandler(object @event)
        {
            if (@event == null)
            {
                return false;
            }

            return _handledTypes.Any(t => t.Key.IsInstanceOfType(@event) && t.Value(@event));
        }
    }
}